package at.amin.slick;

import java.awt.Shape;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tests.AnimationTest;

public class FirstGame extends BasicGame {
	
	private int x_circle;
	private double x_rectangle;
	private double y_rectangle;
	private double y_Square;
	private int moveDirection = 0;
	private int status;
	
	
	public FirstGame() {
	super("First Game");
		
	}

	@Override
	public void render(GameContainer gameContainer, Graphics graphics) throws SlickException {
		
		graphics.setColor(Color.cyan);
		graphics.fillOval(this.x_circle, 100, 50, 50);
		
		graphics.drawRect((int)this.x_rectangle, (int)this.y_rectangle, 100, 50);
		graphics.drawRect(100, (int)this.y_Square, 50, 50);
		

	
	}

	@Override
	public void init(GameContainer gameContainer) throws SlickException {
		this.x_circle = 100;
		this.x_rectangle = 100;
		this.y_rectangle = 100;
		this.y_Square = 100;
		this.status=0;
	}

	@Override
	public void update(GameContainer gameContainer, int delta) throws SlickException {
		if(moveDirection == 0)
		{
			this.x_rectangle += (double) delta *0.5;
		
		}
		else if(moveDirection == 1)
		{
			this.x_rectangle += (double) delta * (-1*0.5);
			
		}
		if(moveDirection == 2)
		{
			this.y_rectangle += (double) delta * 0.5;
			
		}else if(moveDirection == 3)
		{
			this.y_rectangle += (double) delta * (-1*0.5);
			
		}
		
		if(this.status == 0) {
			this.x_circle += (double) delta *0.5;
			if(this.x_circle>=500) {
			this.status=1;
			}
		}
			else if(this.status==1) {
			this.x_circle += (double) delta *(-1*0.5);
			if(this.x_circle<=10) {
				this.status=0;
			}
		}
		if (this.x_rectangle <= 600 && this.y_rectangle <= 50) {
            this.moveDirection = 0;
        } else if (this.x_rectangle >= 600 && this.y_rectangle <= 400) {
            this.moveDirection = 2;
        } else if (this.x_rectangle >= 10 && this.y_rectangle >= 400) {
            this.moveDirection = 1;
        } else if (this.x_rectangle <= 10 && this.y_rectangle >= 30) {
            this.moveDirection = 3;
        }
		
		this.y_Square += (double) delta * 0.5;
		if(this.y_Square > 800)
		{
			this.y_Square = -20;
		}
		
		
		

	}
	
	
	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new FirstGame());
			container.setDisplayMode(800,600,false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

}
