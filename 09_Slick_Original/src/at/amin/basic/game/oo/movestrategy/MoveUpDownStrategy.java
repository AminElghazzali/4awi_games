package at.amin.basic.game.oo.movestrategy;

public class MoveUpDownStrategy implements MoveStrategy {

	private double x,y;

	private int status = 0;

	public MoveUpDownStrategy(double x, double y) {
		super();
		this.x = x;
		this.y = y;
		
		
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	@Override
	public void update(int delta) {
		
		this.y += delta * 0.8;
		
	}

	@Override
	public void setX(float x) {
		this.x = x;
		
	}

	
	
	
	

	
	
	
	
}
