package at.amin.basic.game.oo.movestrategy;

public class MoveLeftRightStrategy implements MoveStrategy {

	private double x,y;

	private int status = 0;

	public MoveLeftRightStrategy(double x, double y) {
		super();
		this.x = x;
		this.y = y;
		
		
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}



	@Override
	public void update(int delta) {
		
		if(this.status == 0) {
			this.x += (double) delta *0.5;
			if(this.x>=500) {
			this.status=1;
			}
		}
			else if(this.status==1) {
			this.x += (double) delta *(-1*0.5);
			if(this.x<=10) {
				this.status=0;
			}
		}
		
	}

	@Override
	public void setY(float y) {
		// TODO Auto-generated method stub
		this.y = y;
		
	}

	@Override
	public void setX(float x) {
		// TODO Auto-generated method stub
		this.x = x;
	}
	
	

	
	
	
	
}
