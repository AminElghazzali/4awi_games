package at.amin.basic.game.oo.movestrategy;

public interface MoveStrategy {
	public void update(int delta);
	public double getX();
	public double getY();
	public void setY(float y);
	public void setX(float x);

}
