package at.amin.basic.game.oo;

import java.awt.Shape;
import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tests.AnimationTest;

import at.amin.basic.game.oo.actors.Actors;
import at.amin.basic.game.oo.actors.Bullet;
import at.amin.basic.game.oo.actors.Obstacle;
import at.amin.basic.game.oo.actors.Oval;
import at.amin.basic.game.oo.actors.Rectangle;
import at.amin.basic.game.oo.actors.Snowflake;
import at.amin.basic.game.oo.actors.StarShip;
import at.amin.basic.game.oo.movestrategy.MoveLeftRightStrategy;
import at.amin.basic.game.oo.movestrategy.MoveStrategy;
import at.amin.basic.game.oo.movestrategy.MoveUpDownStrategy;

public class FirstGame extends BasicGame {
	private List<Actors> actors;
	public static StarShip starship;
	private Obstacle circle;
	private Snowflake snowflake;
	private MoveStrategy mls;
	
	
	
	
	public FirstGame() {

		super("First Game");

	}

	public void addSnowflake(Actors snowflake) {
		this.actors.add(snowflake);
	}

	@Override
	public void render(GameContainer gameContainer, Graphics g) throws SlickException {
		for (Actors actor : actors) {

			actor.render(g);
		}
	}


	@Override
	public void init(GameContainer gameContainer) throws SlickException {
		this.actors = new ArrayList<>();
		starship = new StarShip();

		MoveStrategy uds = new MoveUpDownStrategy(-400, 10);
		
		

//		for (int i = 0; i < 50; i++) {
//			this.actors.add(new Snowflake(0.4, Snowflake.size.medium));
//
//		}
//		for (int i = 0; i < 50; i++) {
//			this.actors.add(new Snowflake(0.3, Snowflake.size.big));
//
//		}
		
		
		
	//	this.actors.add(new Rectangle(200, 200,uds));
		Obstacle c = new Obstacle(2000,50,uds);
		this.actors.add(c);
		starship.setObstacle(c);
	//	this.actors.add(new Circle(500,20,Move1));
    	//this.actors.add(new Oval(50, 50, mls));
//		this.actors.add(new ShootingStar(0.9, 5));
    	this.actors.add(starship);
    	
	}


	@Override
	public void update(GameContainer gameContainer, int delta) throws SlickException {
		
		for (Actors actor : actors) {

			actor.update(delta);
		}
		
		
		

	}
	
	@Override
	public void keyPressed(int key, char c) {
		// TODO Auto-generated method stub
		super.keyPressed(key, c);
		if(key == Input.KEY_LEFT)
		{
			this.starship.isMovingLeft();
			
			
		}
		else if(key == Input.KEY_RIGHT)
		{
			this.starship.isMovingRight();
		}
		else if(key == Input.KEY_SPACE)
		{
			//this.actors.add(new Bullet(this.starship.getX(),this.starship.getY()));
			
		}
		else if(key == Input.KEY_A)
		{
			this.starship.isMovingRight2();
			
			
		}
	}
	@Override
	public void keyReleased(int key, char c) {
		// TODO Auto-generated method stub
		super.keyReleased(key, c);
		if(key == Input.KEY_LEFT)
		{
			this.starship.isnotMovingLeft();
		}
		else if(key == Input.KEY_RIGHT)
		{
			this.starship.isnotMovingRight();
		} 
		else if(key == Input.KEY_A)
		{
			this.starship.isMovingRight3();
			
			
		}
	
		
	}
	
	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new FirstGame());
			container.setDisplayMode(1920,1080, false);
			container.setTargetFrameRate(60);
			container.start();
		
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

}
