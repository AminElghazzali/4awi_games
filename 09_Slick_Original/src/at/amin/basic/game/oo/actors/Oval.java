package at.amin.basic.game.oo.actors;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Shape;

import at.amin.basic.game.oo.movestrategy.MoveStrategy;

public class Oval implements Actors {

	private MoveStrategy mls;
	private int with, height;
	
	public Oval( int with, int height, MoveStrategy mls) {
		super();
		this.mls = mls;
		this.with = with;
		this.height = height;
	}
	
	
	public void update(int delta)
	{
		
		this.mls.update(delta);
	}
	
	public void render(Graphics g)
	{
		g.drawOval((int)this.mls.getX(),(int)this.mls.getY(),10,100);
		
	}


	@Override
	public Shape getShape() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Object getCollisionobject() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	
	
}

