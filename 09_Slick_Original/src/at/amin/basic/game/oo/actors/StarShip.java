package at.amin.basic.game.oo.actors;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

public class StarShip implements Actors {

	private float x = 500;
	private float y = 500;
	private List<Snowflake> snowflakes;
	private ArrayList<Shape> shapes;
	private Obstacle obstacle;
	private boolean isMoving;
	private boolean goRight;
	private boolean goRight2=false;
	private boolean goLeft;
	private Shape shape;
	private int collision = 0;
	private Image car;


	public StarShip() {
		super();
		this.x = 900;
		this.y = 900;
		this.shapes = new ArrayList<>();
		this.shape = new Rectangle(x, y, 20, 20);
//		try {
//			this.car = new Image("testdata/Spaceship.jpg");
//		} catch (SlickException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}

	}

	public void addSnowflake(Snowflake s) {
		this.snowflakes.add(s);
	}

	public void addShape(Shape sh) {
		this.shapes.add(sh);
	}



	@Override
	public void update(int delta) {

		if (goRight == true && goRight2 == true ) {
			x += delta * 0.2;
			
		}
		else if (goRight == true) {
			
			x += delta * 0.9;
		}
		else if (goLeft == true && goRight2 == true) {
			x -= delta * 0.2;

		}
		else if (goLeft == true) {
		
			x -= delta * 0.9;

		}
		

		this.shape.setX(x);

		for (Shape sh : shapes) {
			if (this.shape.intersects(sh)) {
				System.out.println("Kollission" + sh.getX());
				obstacle.setHidden(true);
			} else {
				//obstacle.setHidden(true);
			}
		}

	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
		
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public void isMovingRight() {
		this.goRight = true;

	}
	public void isMovingRight2() {
		this.goRight2 = true;

	}
	public void isMovingRight3() {
		this.goRight2 = false;

	}

	public void isnotMovingRight() {
		this.goRight = false;

	}
	

	public void isMovingLeft() {
		this.goLeft = true;

	}

	public void isnotMovingLeft() {
		this.goLeft = false;
	}

	public void goRight() {
		this.goRight = true;

	}

	public void goLeft() {
		this.goRight = false;

	}

	public void Stop() {
		this.isMoving = false;

	}

	public void setObstacle(Obstacle c) {
		this.obstacle = c;
	}

	public boolean collide(Snowflake snowflake) {
//		
//		if(this.x < this.snowflake.getSnowflake_x() ) return false;
//		if(this.y < this.snowflake.getSnowflake_y() ) return false;
//		if(this.snowflake.getSnowflake_x() > this.x) return false;
//		if(this.snowflake.getSnowflake_y()> this.y ) return false;
		return true;
	}

	@Override
	public void render(Graphics graphics) {
		// TODO Auto-generated method stub
		graphics.drawRect((int) this.x, (int) this.y, 20, 20);
		//this.car.draw((int) this.x, (int) this.y, 100, 100);
		graphics.draw(this.shape);
	}

	@Override
	public Shape getShape() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getCollisionobject() {
		// TODO Auto-generated method stub
		return null;
	}

}
