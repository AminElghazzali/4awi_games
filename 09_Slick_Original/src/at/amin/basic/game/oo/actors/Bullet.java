package at.amin.basic.game.oo.actors;


import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Shape;

public class Bullet implements Actors {

	private double x,y;
	private Image bulletImage;
	
	
	
	public Bullet(double x, double y) {
		super();
		this.x = x;
		this.y = y;
//		try {
//			
////			this.bulletImage = new Image("pictures/spaceshuttle.png");
//			
//		}catch(SlickException e)
//		{
//			e.printStackTrace();
//		}
	}
	
	

	@Override
	public void update(int delta) {
		
	 this.y--;

	}
	

	public double getX() {
		return x;
	}


	public void setX(double x) {
		this.x = x;
	}


	public double getY() {
		return y;
	}


	public void setY(double y) {
		this.y = y;
	}


	@Override
	public void render(Graphics graphics) {
		
		graphics.drawRect((int) this.x,(int) this.y , 20, 20);
//		 this.bulletImage.draw((int) this.x, (int)this.y);
	}



	@Override
	public Shape getShape() {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public Object getCollisionobject() {
		// TODO Auto-generated method stub
		return null;
	}

}
