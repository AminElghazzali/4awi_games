package at.amin.basic.game.oo.actors;

import org.newdawn.slick.geom.Shape;

public interface Enemy {

	public Shape getShape();
	public Object getCollisionobject();
}
