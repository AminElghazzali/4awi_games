package at.amin.basic.game.oo.actors;

import java.util.Random;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Shape;



public class Snowflake implements Actors {

	
	private float snowflake_y, snowflake_x;
	private int status = 0;
	private int size;
	private double speed;
	private Shape shape;
	public enum size{small, medium, big}
	private boolean isHidden = false;
	
	public Snowflake(double speed, size size) {
		super();
	
		this.speed = speed;
		
		if(size == size.small)
		{
			this.size = 15;
		}
		else if (size == size.medium)
		{
			this.size = 8;
			
		}	
		else
		{
			this.size = 10;	
		}
		
		this.shape = new org.newdawn.slick.geom.Circle(this.snowflake_x, this.snowflake_y, this.size/2);
		
		setRandomPosition();
	}
	private void setRandomPosition()
	{
		Random r = new Random();
		this.snowflake_y = r.nextInt(600) - 600;
		this.snowflake_x = r.nextInt(800);
		this.shape.setX(this.snowflake_x);
		this.shape.setY(this.snowflake_y);
	}
	
	
	public double getSnowflake_y() {
		return snowflake_y;
	}
	public void setSnowflake_y(float snowflake_y) {
		this.snowflake_y = snowflake_y;
	}
	public double getSnowflake_x() {
		return snowflake_x;
	}
	public void setSnowflake_x(float snowflake_x) {
		this.snowflake_x = snowflake_x;
	}
	public void update(int delta)
	{
		
		
		this.snowflake_y += (double) delta * this.speed;
		if(this.snowflake_y > 800)
		{
			setRandomPosition();
		}
		
		this.shape.setY(this.snowflake_y);
		
	}
	
	public void render(Graphics g)
	{
		if (isHidden) {
	
			
		}
		
		g.drawOval((int) this.snowflake_x,(int) this.snowflake_y,this.size,this.size);
		g.fillOval((int) this.snowflake_x,(int) this.snowflake_y,this.size,this.size);
		
		g.setColor(Color.white);
		
		//g.draw(this.shape);
		
	
	}
	public Shape getShape() {
		return shape;
	}
	
	public void hide() {
		this.isHidden = true;
	}
	public boolean isHidden() {
		return isHidden;
	}
	public void setHidden(boolean isHidden) {
		this.isHidden = isHidden;
	}
	@Override
	public Object getCollisionobject() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	
}
