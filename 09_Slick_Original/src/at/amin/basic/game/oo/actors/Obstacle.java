package at.amin.basic.game.oo.actors;

import java.util.Random;



import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

import at.amin.basic.game.oo.FirstGame;
import at.amin.basic.game.oo.movestrategy.MoveStrategy;

public class Obstacle implements Actors {
	private MoveStrategy mls;
	private int with, height;
	private int t = -100;
	private Shape shape;
	private Shape shape1;
	private int timeSinceCrash = 0;
	private int survivedTime = 0;
	private int highScore = 0;
	private StarShip starship;
	private int speed = 0;
	private int runTime = 0;

	private boolean isChrashed = false;

	public Obstacle(int with, int height, MoveStrategy mls) {
		super();
		this.mls = mls;
		this.with = with;
		this.height = height;

		this.shape = new Rectangle((int) this.mls.getX(), (int) this.mls.getY(), this.with, this.height);
		this.shape1 = new Rectangle((int) this.mls.getX() + 2080, (int) this.mls.getY(), this.with, this.height);
		FirstGame.starship.addShape(shape);
		FirstGame.starship.addShape(shape1);
		
	}

	public void update(int delta) {
		if (isChrashed) {
			this.timeSinceCrash += delta;
			this.mls.setY(this.t);
			this.survivedTime = 0;

		}
		
		
		
		
		this.mls.update(delta);
		if (this.mls.getY() > 1000) {
			this.mls.setY(this.t);
			this.survivedTime += 1;
			if(this.highScore < this.survivedTime)
			{
				this.highScore = this.survivedTime;
			}

			this.mls.setX(getRandomPosition());

		}
		this.shape.setX((float) this.mls.getX());
		this.shape.setY((float) this.mls.getY());
		this.shape1.setX((float) this.mls.getX() + 2100);
		this.shape1.setY((float) this.mls.getY());

	}

	private int getRandomPosition() {
		Random r = new Random();
		return -1500 + r.nextInt(1000);

	}

	public Shape getShape() {
		return this.shape;

	}

	public Shape getShape1() {
		return this.shape1;

	}

	public void hide() {
		this.isChrashed = true;
	}

	public boolean isHidden() {
		return isChrashed;
	}

	public void setHidden(boolean isHidden) {
		System.out.println("setting hidden");
		this.isChrashed = isHidden;
	}

	public void render(Graphics g) {
		if (isChrashed) {
			g.setColor(Color.red);

			g.drawString("you died", 900, 500);
			
			
			if (this.timeSinceCrash > 2000) {

				this.timeSinceCrash = 0;
				this.isChrashed = false;
			}
		} else {
			
			g.drawString("Survived: "+ this.survivedTime,10, 60);

		}

		g.draw(this.shape);
		g.draw(this.shape1);
		g.setColor(Color.white);
		g.drawString("Highscore: "+ this.highScore, 10, 40);
		
		
	}

	@Override
	public Object getCollisionobject() {

		return this;
	}
}
