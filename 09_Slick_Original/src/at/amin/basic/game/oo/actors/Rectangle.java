package at.amin.basic.game.oo.actors;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Shape;

import at.amin.basic.game.oo.movestrategy.MoveStrategy;

public class Rectangle implements Actors {

		private int with, height;
		private int moveDirection = 0;
		private MoveStrategy mls;
		
		public Rectangle(int with, int height, MoveStrategy mls) {
			super();
			
			this.with = with;
			this.height = height;
		}
		
		
		public void update(int delta)
		{
			
			this.mls.update(delta);
		}
		
		public void render(Graphics g)
		{
			
			g.drawRect((int)this.mls.getX(),(int)this.mls.getY(), 50, 50);
			
		}


		@Override
		public Shape getShape() {
			// TODO Auto-generated method stub
			return null;
		}


		@Override
		public Object getCollisionobject() {
			// TODO Auto-generated method stub
			return null;
		}
	
}
