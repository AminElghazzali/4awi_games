package at.amin.basic.game.oo.actors;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Shape;

public interface Actors {
	
	public void update(int delta);
	
	public void render(Graphics graphics);
	
	public Shape getShape();
	public Object getCollisionobject();


}
