package at.amin.basic.game.oo.actors;
import java.util.Random;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Shape;

public class ShootingStar implements Actors{

	private double x_ShootingStar;
	private double y_ShootingStar;
	private float size;
	private double speed;
	public ShootingStar(double speed, int size ) {
		super();
		this.speed = speed;
		this.size = size;
		setRandomPosition();
		
	}
	@Override
	
	public void update(int delta) {
	
		this.x_ShootingStar += (double) delta * 0.5;
		if(this.x_ShootingStar > 800)
		{
			setRandomPosition();
			setRandomSize();
		}
		
		if(this.x_ShootingStar > 0)
		{
			
		
		for(int i = 0; i < 500; i++)
		{
			this.size = (float) (this.size + 0.001);
		}
		}
	}
	
	private void setRandomPosition()
	{
		Random r = new Random();
		this.y_ShootingStar = r.nextInt(400);
		this.x_ShootingStar = r.nextInt(100)-5000;
	}
	private void setRandomSize()
	{
		Random r2 = new Random();
		this.size = r2.nextInt(16);
	}
	
	
	

	@Override
	public void render(Graphics graphics) {
		
		graphics.setColor(Color.yellow);
		graphics.fillOval((int) this.x_ShootingStar, (int) this.y_ShootingStar, size, size);

		graphics.drawOval((int) this.x_ShootingStar, (int) this.y_ShootingStar, size, size);
		

	}
	@Override
	public Shape getShape() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Object getCollisionobject() {
		// TODO Auto-generated method stub
		return null;
	}

	
}
