package NewGame;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

public class StarShip implements Actors {

	private float x = 500;
	private float y = 500;
	private List<Snowflake> snowflakes;
	private boolean isMoving;
	private boolean goRight;
	private boolean goLeft;
	private Shape shape;
	
	public StarShip() {
		super();
		this.x = 500;
		this.y = 500;
		this.snowflakes = new ArrayList<>();
		this.shape = new Rectangle(x, y, 40, 40);
		
	}
	
	public void addSnowflake(Snowflake s) {
		this.snowflakes.add(s);
	}

	@Override
	public void update(int delta) {

		if (goRight == true) {
			x += delta * 0.5;

		}
		if (goLeft == true) {
			x -= delta * 0.5;

		}
		
		this.shape.setX(x);
		
		
		for (Snowflake snowflake : snowflakes) {
			if (snowflake.getShape().intersects(this.shape)) {
				System.out.println("Kollission");
				snowflake.setHidden(true);
			}
		}
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public  float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public void isMovingRight() {
		this.goRight = true;

	}

	public void isnotMovingRight() {
		this.goRight = false;

	}

	public void isMovingLeft() {
		this.goLeft = true;

	}

	public void isnotMovingLeft() {
		this.goLeft = false;
	}

	public void goRight() {
		this.goRight = true;

	}

	public void goLeft() {
		this.goRight = false;

	}

	public void Stop() {
		this.isMoving = false;

	}

	public boolean collide(Snowflake snowflake) {
//		
//		if(this.x < this.snowflake.getSnowflake_x() ) return false;
//		if(this.y < this.snowflake.getSnowflake_y() ) return false;
//		if(this.snowflake.getSnowflake_x() > this.x) return false;
//		if(this.snowflake.getSnowflake_y()> this.y ) return false;
		return true;
	}

	@Override
	public void render(Graphics graphics) {
		// TODO Auto-generated method stub
		graphics.drawRect((int) this.x, (int) this.y, 20, 20);
	}

}
